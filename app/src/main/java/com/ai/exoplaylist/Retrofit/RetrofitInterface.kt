package com.ai.exoplaylist.Retrofit

import com.ai.exoplaylist.Model.Videos
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface RetrofitInterface {

    @GET("recipes.php?")
    fun getAllMovies(@QueryMap paramsMap: Map<String, String>): Call<Videos>
}