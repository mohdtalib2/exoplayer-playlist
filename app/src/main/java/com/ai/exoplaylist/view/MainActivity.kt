package com.ai.exoplaylist.view

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.media.MediaFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.MotionEvent.*
import android.widget.*
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.*
import coil.compose.rememberImagePainter
import com.ai.exoplaylist.Model.VideoResponse
import com.ai.exoplaylist.R
import com.ai.exoplaylist.viewmodel.MainActivityViewModel
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.rtsp.RtspMediaSource
import com.google.android.exoplayer2.text.Cue
import com.google.android.exoplayer2.trackselection.*
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.style.TextOverflow
import androidx.core.content.ContextCompat
import coil.annotation.ExperimentalCoilApi
import com.ai.exoplaylist.ui.theme.*
import com.google.android.exoplayer2.util.MimeTypes
import kotlinx.coroutines.launch
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.C.ROLE_FLAG_CAPTION
import com.google.android.exoplayer2.C.SELECTION_FLAG_FORCED
import com.google.android.exoplayer2.text.Subtitle
import com.google.android.exoplayer2.text.TextRenderer

import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector.ParametersBuilder


class MainActivity : ComponentActivity() {

    private var mediaSources = mutableListOf<MediaSource>()
    private lateinit var exoPlayer: ExoPlayer
    private val viewModel: MainActivityViewModel by viewModels()
    lateinit var selectedItemIndex:MutableState<Int>
    private lateinit var fullScreen:MutableState<Boolean>
    private lateinit var isControlsVisible:MutableState<Boolean>
    private lateinit var fullScreenButton:ImageView
    private lateinit var playerView:PlayerView
    private lateinit var seekLine:LinearLayout
    private lateinit var caption:ImageView
    private lateinit var view:View
    @ExperimentalMaterialApi
    private lateinit var bottomSheetScaffoldState:BottomSheetScaffoldState
    private var videoList = mutableListOf<VideoResponse>()


    @ExperimentalCoilApi
    @ExperimentalComposeUiApi
    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

         view = LayoutInflater.from(this)
            .inflate(R.layout.surface, null, false)
        fullScreenButton = view.findViewById<ImageView>(R.id.exo_fullscreen)

        setContent {
            ExoplaylistTheme {
                fullScreen = remember { mutableStateOf(false) }
                isControlsVisible = remember { mutableStateOf(false) }
                bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
                    bottomSheetState = BottomSheetState(BottomSheetValue.Collapsed)
                )
                var offsetX by remember { mutableStateOf(0f) }
                var offsetY by remember { mutableStateOf(0f) }

                val coroutineScope = rememberCoroutineScope()
                BottomSheetScaffold(
                    scaffoldState = bottomSheetScaffoldState,
                    sheetContent = {
                        Box(modifier = Modifier
                            .fillMaxWidth()
                            .height(200.dp)
                            .background(black)

                        ) {
                           LazyRow(modifier = Modifier
                               .padding(0.dp, 5.dp, 0.dp, 5.dp)
                               .fillMaxWidth()
                               .background(black)
                               .fillMaxHeight(),
                           verticalAlignment = Alignment.CenterVertically) {
                               itemsIndexed(videoList) { index, video ->
                                   BottomPlaylistRow(video, index)
                               }
                           }
                        }
                    }, sheetPeekHeight = if (fullScreen.value && !isControlsVisible.value) 30.dp else 0.dp,
                    sheetElevation = 10.dp,
                    sheetGesturesEnabled = true
                ) {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(darkBackground)
                            .pointerInput(Unit) {
                                detectDragGestures { change, dragAmount ->
                                    change.consumeAllChanges()

                                    val (x, y) = dragAmount
                                    when {
                                        x > 1000 -> {
                                        }
                                        x < -1000 -> {
                                        }
                                    }
                                    when {
                                        y > 0 -> { /* down */
                                        }
                                        y < -50 -> {

                                            coroutineScope.launch {
                                                if (fullScreen.value) {
                                                    if (bottomSheetScaffoldState.bottomSheetState.isCollapsed) {
                                                        bottomSheetScaffoldState.bottomSheetState.expand()
                                                    } else {
                                                        bottomSheetScaffoldState.bottomSheetState.collapse()
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    offsetX += dragAmount.x
                                    offsetY += dragAmount.y
                                }
                            }

                    ) {
                        selectedItemIndex = remember { mutableStateOf(0) }

                        Column(modifier = Modifier.fillMaxSize()) {
                            VideoPlaylist()
                        }
                    }

                }
            }

            @Suppress("DEPRECATION")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                window.insetsController?.hide(WindowInsets.Type.statusBars())
            } else {
                window.setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
                )
            }
        }


    }

    @ExperimentalMaterialApi
    @SuppressLint("RememberReturnType", "ClickableViewAccessibility")
    @Composable
    fun VideoPlayer(mediaList: List<VideoResponse>) {

        val context = LocalContext.current
        val lifecycle = LocalLifecycleOwner.current.lifecycle

        if (mediaList.isNotEmpty()) {
            for (video in mediaList) {
                mediaSources.add(buildMediaSource(video.videoUrl, video.streamFormat, video.cc_path))
            }
        }

         exoPlayer = remember {
            ExoPlayer.Builder(context)
                .setRenderersFactory(DefaultRenderersFactory(context))
                .setSeekBackIncrementMs(10000)
                .setSeekForwardIncrementMs(10000).build().apply {
                var trackSelector = DefaultTrackSelector.ParametersBuilder(context)
                    .setRendererDisabled(C.TRACK_TYPE_VIDEO, false).build()
                this.trackSelectionParameters = trackSelector
                this.setMediaSources(mediaSources)
                this.prepare()
                this.playWhenReady = true
            }
        }

        AndroidView(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            factory = { ctx ->
                playerView = view.findViewById<PlayerView>(R.id.player_view)
                caption = view.findViewById<ImageView>(R.id.caption)
                seekLine = view.findViewById<LinearLayout>(R.id.seek_line)

                var isCaptionEnabled = true

                caption.setOnClickListener {
                    if (isCaptionEnabled) {
                        exoPlayer.trackSelectionParameters = DefaultTrackSelector.ParametersBuilder(context)
                            .setRendererDisabled(C.TRACK_TYPE_VIDEO, true).build()
                        caption.setImageResource(R.drawable.ic_caption_off)
                        isCaptionEnabled = false

                    }
                    else {
                        exoPlayer.trackSelectionParameters = DefaultTrackSelector.ParametersBuilder(context)
                            .setRendererDisabled(C.TRACK_TYPE_VIDEO, false).build()
                        caption.setImageResource(R.drawable.ic_caption_on)
                        isCaptionEnabled = true
                    }
                }

                if (mediaList[selectedItemIndex.value].cc_path != "") {

                    if (caption.visibility == View.GONE) {
                        caption.visibility = View.VISIBLE
                    }
                }
                else {
                    if (caption.visibility == View.VISIBLE) {
                        caption.visibility = View.GONE
                    }
                }

                playerView.player = exoPlayer
                playerView.setOnClickListener {
                    if (fullScreen.value) {
                        isControlsVisible.value = playerView.isControllerVisible
                    }
                }

                view // return the view
            }
        )


        fullScreenButton.setOnClickListener {
            if (fullScreen.value) {
                fullScreenButton.setImageResource(R.drawable.ic_fullscreen)
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                val params = playerView.layoutParams as RelativeLayout.LayoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = (200 * applicationContext.resources.displayMetrics.density).toInt()
                playerView.layoutParams = params

                val params1 = seekLine.layoutParams as RelativeLayout.LayoutParams
                params1.width = ViewGroup.LayoutParams.MATCH_PARENT
                params1.height = ViewGroup.LayoutParams.WRAP_CONTENT
                params1.setMargins(0, 0, 0 , 0)
                seekLine.layoutParams = params1

                fullScreen.value = false
            }
            else {
                fullScreenButton.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@MainActivity,
                        R.drawable.ic_fullscreen_exit
                    )
                )
                window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)

                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                val params = playerView.layoutParams as RelativeLayout.LayoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = ViewGroup.LayoutParams.MATCH_PARENT
                playerView.layoutParams = params

                val params1 = seekLine.layoutParams as RelativeLayout.LayoutParams
                params1.width = ViewGroup.LayoutParams.MATCH_PARENT
                params1.height = ViewGroup.LayoutParams.WRAP_CONTENT
                params1.setMargins(0, 0, 0 ,
                    (35 * applicationContext.resources.displayMetrics.density).toInt())
                seekLine.layoutParams = params1

                fullScreen.value = true
            }
        }

        exoPlayer.addListener(object : Player.Listener {

            override fun onTracksInfoChanged(tracksInfo: TracksInfo) {
                super.onTracksInfoChanged(tracksInfo)
                selectedItemIndex.value = exoPlayer.currentMediaItemIndex

                if (mediaList[selectedItemIndex.value].cc_path != "") {

                    if (caption.visibility == View.GONE) {
                        caption.visibility = View.VISIBLE
                    }
                }
                else {

                    if (caption.visibility == View.VISIBLE) {
                        caption.visibility = View.GONE
                    }
                }
            }

            override fun onCues(cues: MutableList<Cue>) {
                super.onCues(cues)

            }

        })

        val lifecycleOwner by rememberUpdatedState(LocalLifecycleOwner.current)
        DisposableEffect(lifecycleOwner) {
            val lifecycle = lifecycleOwner.lifecycle
            val observer = LifecycleEventObserver { _, event ->
                when (event) {
                    Lifecycle.Event.ON_PAUSE -> {
                        exoPlayer.playWhenReady = false
                    }
                    Lifecycle.Event.ON_RESUME -> {
                        exoPlayer.playWhenReady = true
                    }
                    Lifecycle.Event.ON_DESTROY -> {
                        exoPlayer.run {
                            stop()
                            release()
                        }
                    }
                }
            }
            lifecycle.addObserver(observer)
            onDispose {
                lifecycle.removeObserver(observer)
            }
        }


    }


    private fun buildMediaSource(
        videoUrl: String,
        streamFormat: String,
        cc_path: String
    ): MediaSource {

        val dataSourceFactory: DataSource.Factory = DefaultHttpDataSource.Factory()

        var subtitleConfiguration:MediaItem.SubtitleConfiguration
        var subtitleMediaSource:SingleSampleMediaSource

        val mediaItem: MediaItem = MediaItem.Builder()
            .setUri(videoUrl)
            .build()

        var mediaSource = when (streamFormat) {
            "hls" -> {
                HlsMediaSource.Factory(dataSourceFactory)
                    .setAllowChunklessPreparation(true)
                    .createMediaSource(mediaItem)
            }

            "dash" -> {
                DashMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(mediaItem)
            }

            "mp4" -> {
                ProgressiveMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(mediaItem)
            }

            "rtsp" -> {
                RtspMediaSource.Factory()
                    .createMediaSource(mediaItem)
            }
            else -> {
                ProgressiveMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(mediaItem)
            }
        }

        if (cc_path != "") {

                subtitleConfiguration = MediaItem.SubtitleConfiguration
                    .Builder(Uri.parse(cc_path))
                    .setMimeType(MimeTypes.APPLICATION_SUBRIP)
                    .setLanguage("en")
                    .setSelectionFlags(SELECTION_FLAG_FORCED)
                    .setRoleFlags(ROLE_FLAG_CAPTION)
                    .build()

                subtitleMediaSource = SingleSampleMediaSource.Factory(dataSourceFactory)
                    .setTreatLoadErrorsAsEndOfStream(true)
                    .createMediaSource(subtitleConfiguration, C.TIME_UNSET)

            return MergingMediaSource(true, mediaSource, subtitleMediaSource)
        }

        return mediaSource
    }

    @ExperimentalMaterialApi
    @Composable
    fun VideoPlaylist() {

        var hidden by remember{ mutableStateOf(false) }

        if (!hidden) {

            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                CircularProgressIndicator(
                    color = Color.White
                )
            }
        }

        val videoList1 by viewModel.getAllVideos().observeAsState(listOf())
        if (videoList1.isNotEmpty()) {
            videoList = videoList1 as MutableList<VideoResponse>
        }

        if (videoList1.isNotEmpty()) {
            VideoPlayer(videoList1)
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(0.dp, 10.dp, 0.dp, 5.dp)

            ) {
                itemsIndexed(videoList1) { index, video ->
                    PlaylistRow(video, index)
                }

            }

            hidden = true
        }
    }

    @Composable
    fun PlaylistRow(video: VideoResponse, index: Int) {

        Column(modifier = Modifier
            .padding(0.dp, 10.dp, 0.dp, 5.dp)
            .background(
                if (selectedItemIndex.value == index)
                    selectedItemBackground else darkBackground
            )
            .clickable {
                if (exoPlayer.currentMediaItemIndex != index)
                    exoPlayer.seekTo(index, 0)
            },
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {

            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(180.dp),
                contentScale = ContentScale.Crop,
                painter = rememberImagePainter(data = video.xhd_image,
                    builder = {
                        crossfade(false)
                        placeholder(R.drawable.placeholder)
                    }),
                contentDescription = null,
            )

            Text(modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 5.dp, 10.dp, 0.dp),
                text = video.title,
                color = Color.White,
                style = Typography.body1
            )

            Text(modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 1.dp, 10.dp, 0.dp),
                text = video.date,
                style = Typography.body2
            )

        }

    }

    @ExperimentalCoilApi
    @Composable
    fun BottomPlaylistRow(video: VideoResponse, index: Int) {

        Column(modifier = Modifier
            .padding(10.dp, 0.dp, 10.dp, 0.dp)
            .fillMaxWidth()
            .background(
                if (selectedItemIndex.value == index)
                    selectedItemBackground else black
            )
            .clickable {
                if (exoPlayer.currentMediaItemIndex != index)
                    exoPlayer.seekTo(index, 0)
            }) {

            Image(
                modifier = Modifier
                    .width(190.dp)
                    .height(140.dp),
                contentScale = ContentScale.Crop,
                painter = rememberImagePainter(data = video.xhd_image,
                    builder = {
                        crossfade(false)
                        placeholder(R.drawable.placeholder)
                    }),
                contentDescription = null,
            )

            Text(modifier = Modifier
                .width(190.dp)
                .padding(5.dp, 5.dp, 5.dp, 0.dp),
                text = video.title,
                color = Color.White,
                style = Typography.body1,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )

        }

    }

    @ExperimentalFoundationApi
    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview() {

    }

    override fun onBackPressed() {
        if (fullScreen.value) {
            fullScreenButton.setImageResource(R.drawable.ic_fullscreen)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            val params = playerView.layoutParams as RelativeLayout.LayoutParams
            params.width = ViewGroup.LayoutParams.MATCH_PARENT
            params.height = (200 * applicationContext.resources.displayMetrics.density).toInt()
            playerView.layoutParams = params

            val params1 = seekLine.layoutParams as RelativeLayout.LayoutParams
            params1.width = ViewGroup.LayoutParams.MATCH_PARENT
            params1.height = ViewGroup.LayoutParams.WRAP_CONTENT
            params1.setMargins(0, 0, 0 , 0)
            seekLine.layoutParams = params1

            fullScreen.value = false
        }
        else {
            super.onBackPressed()
        }

    }
}
