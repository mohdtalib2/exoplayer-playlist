package com.ai.exoplaylist.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ai.exoplaylist.Model.VideoResponse
import com.ai.exoplaylist.repository.VideoListRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityViewModel: ViewModel() {

    private var liveData: MutableLiveData<List<VideoResponse>>
    private var videoListRepository: VideoListRepository

    init {
        liveData = MutableLiveData()
        videoListRepository = VideoListRepository()
    }

    fun getAllVideos(): MutableLiveData<List<VideoResponse>> {
        liveData = videoListRepository.makeApiCall()
        return liveData
    }

}
