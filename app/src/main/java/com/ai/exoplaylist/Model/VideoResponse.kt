package com.ai.exoplaylist.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class VideoResponse(
    @SerializedName("title")
    @Expose
    var title:String,

    @SerializedName("date")
    @Expose
    var date:String,

    @SerializedName("xhd_image")
    @Expose
    var xhd_image:String,

    @SerializedName("video_url")
    @Expose
    var videoUrl:String,

    @SerializedName("streamFormat")
    @Expose
    var streamFormat:String,

    @SerializedName("cc_path")
    @Expose
    var cc_path:String,

)
