package com.ai.exoplaylist.Model

sealed class ApiState {
    class Success(val data: List<VideoResponse>): ApiState()
    class Failure(val msg: Throwable): ApiState()
    object Loading: ApiState()
    object Empty: ApiState()


}
