package com.ai.exoplaylist.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Videos(
    @SerializedName("status")
    @Expose
    val status:String,
    @SerializedName("count")
    @Expose
    val count:Int,
    @SerializedName("results")
    @Expose
    val videosList: List<VideoResponse>
)
