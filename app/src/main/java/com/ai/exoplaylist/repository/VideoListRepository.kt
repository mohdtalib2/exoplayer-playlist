package com.ai.exoplaylist.repository

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.MutableLiveData
import com.ai.exoplaylist.Model.VideoResponse
import com.ai.exoplaylist.Model.Videos
import com.ai.exoplaylist.Retrofit.RetrofitInstance
import com.ai.exoplaylist.Retrofit.RetrofitInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Query
import java.lang.Exception

class VideoListRepository {

    fun makeApiCall(): MutableLiveData<List<VideoResponse>> {
         var map = mutableMapOf<String, String>()
        map["searchType"] = "new-qid";
        map["keys"] = "34449";
        map["appId"] = "4";
        map["siteId"] = "1095";
        map["auth-token"] = "1212551";
        map["version"] = "sv6.0";
        map["sort_type"] = "";
        map["order"] = "0";
        map["deviceModel"] = "";
        map["deviceId"] = "";
        map["parent"] = "Home"
        map["showType"] = "listoflist";
        map["showKeys"] = "32294";
        map["akp"] = "2411-69865-32294-34449";
        var videoListData = MutableLiveData<List<VideoResponse>>()
        try {
        val retrofitInstance = RetrofitInstance.getInstance()
        val retrofitService = retrofitInstance.create(RetrofitInterface::class.java)
        val call: Call<Videos> = retrofitService.getAllMovies(map)
        call.enqueue(object : Callback<Videos> {
            override fun onResponse(call: Call<Videos>, response: Response<Videos>) {

                videoListData.postValue(response.body()!!.videosList)

            }

            override fun onFailure(call: Call<Videos>, t: Throwable) {
                t.printStackTrace();
            }
        })

        }catch (e: Exception) {
            e.printStackTrace()
        }

        return videoListData
    }


}