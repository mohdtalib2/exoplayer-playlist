package com.ai.exoplaylist.ui.theme

import androidx.compose.ui.graphics.Color

val darkGrey = Color(0xFF302E30)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val grey = Color(0xFF8B8787)
val darkBackground = Color(0xFF121212);
val selectedItemBackground = Color(0xFF615F5F);
val black = Color(0xFF000000);